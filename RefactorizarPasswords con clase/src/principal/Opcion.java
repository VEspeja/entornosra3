package principal;

import java.util.Scanner;

public class Opcion {
	private int opcion;
	private int longitud;
	private String password;
	Opcion(){
		this.opcion=0;
	}
	Opcion(int opcion){
		this.opcion=opcion;
	}
	/**
	 * Este metodo pregunta al usuario el tipo de contraseña que quiere generar
	 * y la longitud de esta
	 * @return un int que se usara como la longitud  del password
	 */
	void preguntaOpcion() {
		Scanner scanner=new Scanner(System.in);
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		System.out.println("Introduce la longitud de la cadena: ");
		this.longitud = scanner.nextInt();
		this.password = "";
	}
	/**
	 * Este metodo es el que se va a encargar de generar la contraseña
	 * 
	 * @return un string que es el password
	 */
	
	 void generarContraseña() {
		 for (int i = 0; i < longitud; i++) {
		switch (this.opcion) {
		
		case 1:
			generarContraseñaLetra(password, longitud);
			break;
		case 2:
			generarContraseñaNumeros(password, longitud);
			break;
		case 3:
			generarContraseñaLetraYCaracter(password, longitud);
			break;
		case 4:
			generarContraseñaLetraCaracterNumero(password,longitud);
			break;
		}
	}
	 }
	 /**
	  * Este metodo devuelve una contraseña con letras y caracteres
	  * @param password es el parametro que va a generar
	  * @param longitud es el parametro que usa para el tamaño de la contraseña
	  * @return un String que es el password generado
	  */
private static String generarContraseñaLetraYCaracter(String password, int longitud) {
		
			byte n;
			n = (byte) (Math.random() * 2);
			if (n == 1) {
				password += generarLetra();
			} else {
				password += generarCaracter();
			}
		return password;
	}
/**
 *  Este metodo devuelve una contraseña numerica
 * @param password es el parametro que va a generar
 * @param longitud es el parametro que usa para el tamaño de la contraseña
 * @return devuelve un String que es el password con los numeros generados 
 */
	private static String generarContraseñaNumeros(String password, int longitud) {
		
			password += generarNumero();
		return password;
	}
/**
 * Este metodo devuleve una contraseña de letras
 * @param password es el parametro que va a generar
 * @param longitud es el parametro que usa para el tamaño de la contraseña
 * @return devuelve un String que es el password con las letras
 */
	private static String generarContraseñaLetra(String password, int longitud) {
		
			password += generarLetra();
		return password;
	}
/**
 * Este metodo devuelve una contraseña que contiene letras caracteres y numeros	
 * @param password es el parametro que va a generar
 * @param longitud es el parametro que usa para el tamaño de la contraseña
 * @return devuelve un String, el password generado con las letras, caracteres
 * 		   y numeros
 */
	private static String generarContraseñaLetraCaracterNumero(String password,int longitud) {
		byte n;
		n = (byte) (Math.random() * 3);
		if (n == 1) {
			password += generarLetra();
		} else if (n == 2) {
			password += generarCaracter();
		} else {
			password += generarNumero();
		}
	
	return password;
	
}
	/**
	 * Este metodo genera un numero aleatorio
	 * @return un char que es un numero
	 */
	private static char generarNumero() {
		
		return (char) ((Math.random() * 10)+48);
	}
	/**
	 * Este metodo genera un caracter aleatorio
	 * @return un char que es el caracter generado
	 */
	private static char generarCaracter() {
		
		return (char) ((Math.random() * 15) + 33);
	}
	/**
	 * Este metodo genera una letra aleatoria
	 * @return un char que es la letra generada
	 */
	private static char generarLetra() {
		
		return (char) ((Math.random() * 26) + 65);
	}
	public int getOpcion() {
		return opcion;
	}
	public void setOpcion(int opcion) {
		this.opcion = opcion;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}
