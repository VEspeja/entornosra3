/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */


import java.util.Scanner;
// borrar la primera y cambiar al import del paquete que usemos
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizar2 {
	final static int CANTIDAD_MAXIMA_ALUMNOS = 10;
	//cambiar la variable a constante
	static Scanner scanner = new Scanner(System.in);
	// declarar el scanner arriba no dentro del main y cambiar el nombre
	public static void main(String[] args) {

		
		int[] notasAlumnos = new int[CANTIDAD_MAXIMA_ALUMNOS];
		// colocar los [] antes del nombre del vector
		// cambiar el nombre del vector y asignarle el tamaño de la constante
		for(int n = 0 ; n < notasAlumnos.length; n++){
			//declarar la n cuando la usemos
			System.out.println("Introduce nota media de alumno");
			notasAlumnos[n] = scanner.nextLine();
		}	
		
		System.out.println("El resultado es: " + mediaAlumnos(notasAlumnos);
		
		scanner.close;
	}
	static double mediaAlumnos(int[] notasAlumnos){
	// cambiar el nombre del metodo a lo que realmente realiza 
	// colocar los [] antes del nombre del vector y el { seguido del parentesis
	
		double sumaNotas=0;
		//cambiar el nombre de la variable por un nombre que lo representa
		for(int a = 0; a < notasAlumnos.length; a ++) {
			// darle un tamaño del vector no un numero 
			// dar espacios y colocar el { detras del (
			sumaNotas = sumaNotas + notasAlumnos[a];
		}
		return sumaNotas / 10;
	}
	
}