/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea de de bloque.
 */
// eliminar los import y crear solamente el import utilizado
// import java.*;
// import java.util.*;
import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizar1 {
	final static String SALUDO = "Bienvenido al programa";
	// cambiar a mayusculas la constante
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println(SALUDO);
		//llamar correctamente a la constante despues de modificarla
		System.out.println("Introduce tu dni");
		String dni = scanner.nextLine();
		System.out.println("Introduce tu nombre");
		String nombre = scanner.nextLine();
		// asignar un nombre de variable adecuado a cada una para lo que sirve
		int numeroA = 7; 
		int numeroB = 16;
		// crear cada variable cuando vaya a utilizarse y separados los operadores
		int numeroC = 25;
		if((numeroA > numeroB)
		|| (numeroc % 5 != 0 && ((numeroc * 3)- 1)>(b / numeroC))){
		// separar las condiciones con un intro y colocar parentesis para separarlas, 
		//cambiar la llave a despues del parentesis no debajo
			System.out.println("Se cumple la condición");
		}
		
		numeroC = (numeroA + numeroB) * numeroC + (numeroB / numeroA);
		// separar con parentesis las operaciones que no sean multiplicacion
		String[] diasSemana = {"Lunes", "Martes", "Miercoles", 
						"Jueves", "Viernes", "Sabado", "Domingo"};
		// colocar los corchetes antes de darle el nombre a la variable del array
		
		mostrarDiasSemana(diasSemana);
		// no usar _ si no seguido y mayuscula si cambias de palabra
	}
	static void mostrarDiasSemana(String[] diasSemana){
	//cambiar la posicion de los [] a antes de la variable ya que es para decir que es un array de String
	
		for(int i = 0; i < diasSemana.length; i ++) {
			//asignar espacios para ver mas claro y colocar la llave despues del parentesis no debajo 
			System.out.println("El dia de la semana en el que te"
			+" encuentras [" + (i+1) + "-7] es el dia: " + diasSemana[i]);
			// dar espacios para que se vea mas claro
		}
	}
	
}
